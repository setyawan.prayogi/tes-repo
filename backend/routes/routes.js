// import express
import express from "express";
 
// import function from controller
import { showMahasiswa, showMahasiswaById, createMahasiswa, updateMahasiswa, deleteMahasiswa } from "../controllers/mahasiswa.js";
 
// init express router
const router = express.Router();
 
// Get All Product
router.get('/mahasiswas', showMahasiswa);
 
// Get Single Product
router.get('/mahasiswas/:id', showMahasiswaById);
 
// Create New Product
router.post('/mahasiswas', createMahasiswa);
 
// Update Product
router.put('/mahasiswas/:id', updateMahasiswa);
 
// Delete Product
router.delete('/mahasiswas/:id', deleteMahasiswa);
 
// export default router
export default router;