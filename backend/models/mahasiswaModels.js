// import connection
import db from "../config/database.js";
 
// Get All Products
export const getMahasiswa = (result) => {
    db.query("SELECT * FROM mahasiswa", (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
 
// Get Single Product
export const getMahasiswaById = (id, result) => {
    db.query("SELECT * FROM mahasiswa WHERE id_mhs = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });   
}
 
// Insert Product to Database
export const insertMahasiswa = (data, result) => {
    db.query("INSERT INTO mahasiswa SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
 
// Update Product to Database
export const updateMahasiswaById = (data, id, result) => {
    db.query("UPDATE mahasiswa SET nim_mhs = ?, nama_mhs = ?, alamat_mhs = ? WHERE id_mhs = ?", [data.nim_mhs, data.nama_mhs, data.alamat_mhs, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}
 
// Delete Product to Database
export const deleteMahasiswaById = (id, result) => {
    db.query("DELETE FROM mahasiswa WHERE id_mhs = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}