// Import function from Product Model
import { getMahasiswa, getMahasiswaById, insertMahasiswa, updateMahasiswaById, deleteMahasiswaById } from "../models/mahasiswaModels.js";
 
// Get All Products
export const showMahasiswa = (req, res) => {
    getMahasiswa((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
 
// Get Single Product 
export const showMahasiswaById = (req, res) => {
    getMahasiswaById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
 
// Create New Product
export const createMahasiswa = (req, res) => {
    const data = req.body;
    insertMahasiswa(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
 
// Update Product
export const updateMahasiswa = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateMahasiswaById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
 
// Delete Product
export const deleteMahasiswa = (req, res) => {
    const id = req.params.id;
    deleteMahasiswaById(id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}